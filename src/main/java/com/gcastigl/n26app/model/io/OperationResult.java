package com.gcastigl.n26app.model.io;

import static java.util.Objects.requireNonNull;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class OperationResult {

	private static final OperationResult OK = new OperationResult(StatusType.OK, null, null);

	public static OperationResult ok() {
		return OK;
	}

	public static OperationResult error(Enum<?> error) {
		return error(error.getClass().getSimpleName(), error.name());
	}

	public static OperationResult error(String type, String code) {
		return new OperationResult(StatusType.ERROR, type, code);
	}

	public enum StatusType {
		OK, ERROR;
	}

	private StatusType status;
	private String type;
	private String code;

	OperationResult() {
	}

	public OperationResult(StatusType status, String type, String code) {
		this.status = requireNonNull(status);
		this.type = type;
		this.code = code;
	}

	public void setStatus(StatusType status) {
		this.status = status;
	}

	public StatusType getStatus() {
		return status;
	}

	public String getType() {
		return type;
	}

	public String getCode() {
		return code;
	}
	
	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
