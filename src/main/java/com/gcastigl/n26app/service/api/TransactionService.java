package com.gcastigl.n26app.service.api;

import java.util.Optional;
import java.util.Set;

import com.gcastigl.n26app.model.io.TranasctionSum;
import com.gcastigl.n26app.model.io.TransactionRequest;

public interface TransactionService {

	enum TransactionOperationError {
		MISSING_PARAMETERS, INVALID_TRANSACTION_ID, INVALID_AMOUNT, INVALID_TYPE, INVALID_PARENT_ID, UNEXPECTED
	}

	Optional<TransactionOperationError> create(long id, TransactionRequest request);

	Set<Long> findAllByType(String type);

	Optional<TranasctionSum> getSum(Long transactionId);

}
