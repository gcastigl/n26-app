package com.gcastigl.n26app.model.io;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class TransactionRequest {

	public static TransactionRequest with(double amount, String type, Long parentId) {
		TransactionRequest request = new TransactionRequest();
		request.setAmount(amount);
		request.setType(type);
		request.setParent_id(parentId);
		return request;
	}

	private double amount;
	private String type;
	private Long parent_id;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getParent_id() {
		return parent_id;
	}

	public void setParent_id(Long parent_id) {
		this.parent_id = parent_id;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
