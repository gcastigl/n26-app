curl -X PUT localhost:8080/transactionservice/transaction/10 -d '{ "amount": 5000, "type": "cars" }' -H "Content-Type: application/json"
curl -X PUT localhost:8080/transactionservice/transaction/20 -d '{ "amount": 3500, "type": "cars" }' -H "Content-Type: application/json"
curl -X PUT localhost:8080/transactionservice/transaction/11 -d '{ "amount": 10000, "type": "shopping", "parent_id": "10"}' -H "Content-Type: application/json"
curl -X PUT localhost:8080/transactionservice/transaction/12 -d '{ "amount": 2000, "type": "eating", "parent_id": "11"}' -H "Content-Type: application/json"


curl -X GET localhost:8080/transactionservice/types/cars

curl -X GET localhost:8080/transactionservice/sum/10
curl -X GET localhost:8080/transactionservice/sum/11
