package com.gcastigl.n26app.model.io;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class TranasctionSum {

	private double sum;

	TranasctionSum() {
	}

	public TranasctionSum(double sum) {
		this.sum = sum;
	}

	public void setSum(double sum) {
		this.sum = sum;
	}

	public double getSum() {
		return sum;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}
}
