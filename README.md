# Information

__Number 26__ Java Code Challenge

## Technologies
	1. Maven - Dependency management
	2. Spring Boot - Servlet engine, dependency injection, etc.
	3. JUnit, hamcreat - Testing
	4. Jersey - Web server controller
	5. Logback - Logging

# Development

## IDE
	1. Run N26Application.java class

## Console
	1. mvn clean package
	2. java -jar n26app-1.0-SNAPSHOT.jar

# Testing
To run all tests:

	1. mvn clean test

  There is a high concurrency tests at _HighConcurrencyTest_ class that creates a tree with N (configurable) nodes using K (configurable) threads at once. To run this class it is necessary to have a running version of the app listening at localhost:8080