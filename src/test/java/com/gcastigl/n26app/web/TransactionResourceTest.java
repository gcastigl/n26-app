package com.gcastigl.n26app.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.gcastigl.n26app.model.io.OperationResult;
import com.gcastigl.n26app.model.io.TranasctionSum;
import com.gcastigl.n26app.model.io.TransactionRequest;
import com.gcastigl.n26app.service.api.AppServices;
import com.gcastigl.n26app.service.api.TransactionService;
import com.gcastigl.n26app.service.api.TransactionService.TransactionOperationError;

@RunWith(MockitoJUnitRunner.class)
public class TransactionResourceTest {

	@Mock
	private TransactionService transactions;

	@Mock
	private AppServices app;

	@Spy
	@InjectMocks
	private TransactionResource transactionResource;

	@Before
	public void configureApp() {
		when(app.transactions()).thenReturn(transactions);
	}

	@Test
	public void testCreateGracefullExceptionHandling() {
		// When
		doNothing().when(transactionResource).logUnexpectedException(any(Throwable.class));
		when(transactions.create(anyLong(), any(TransactionRequest.class))).thenThrow(new NullPointerException());
		// Then
		OperationResult result = transactionResource.create(1L, new TransactionRequest());
		assertEquals(result.getStatus(), OperationResult.StatusType.ERROR);
		assertEquals(result.getCode(), TransactionOperationError.UNEXPECTED.name());
		verify(transactionResource, times(1)).logUnexpectedException(any(Throwable.class));
	}

	@Test
	public void testCreate() {
		when(transactions.create(anyLong(), any(TransactionRequest.class))).thenReturn(Optional.empty());
		OperationResult result = transactionResource.create(1L, new TransactionRequest());
		assertEquals(result.getStatus(), OperationResult.StatusType.OK);
	}

	@Test
	public void testGetSumGracefullExceptionHandling() {
		// When
		doNothing().when(transactionResource).logUnexpectedException(any(Throwable.class));
		when(transactions.getSum(anyLong())).thenThrow(new NullPointerException());
		// Then
		TranasctionSum result = transactionResource.getSum(1L);
		assertNull(result);
		verify(transactionResource, times(1)).logUnexpectedException(any(Throwable.class));
	}

	@Test
	public void testGetByTypeGracefullExceptionHandling() {
		// When
		doNothing().when(transactionResource).logUnexpectedException(any(Throwable.class));
		when(transactions.findAllByType(anyString())).thenThrow(new NullPointerException());
		// Then
		Collection<Long> result = transactionResource.getTypes("asd");
		assertNull(result);
		verify(transactionResource, times(1)).logUnexpectedException(any(Throwable.class));
	}
}
