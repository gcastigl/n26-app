package com.gcastigl.n26app.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TransactionLockServiceTest {
	
	private static final long waitTimeMs = 100;

	@Spy
	@InjectMocks
	private TransactionLockService locks;

	@Test(timeout = 500)
	public void testSyncronizedAccessToSameId() throws InterruptedException {
		// Given
		Lock thread1AccquireLock = new ReentrantLock();
		Lock thread1ReleaseLock = new ReentrantLock();
		Lock thread2AccquireLock = new ReentrantLock();
		Lock thread2ReleaseLock = new ReentrantLock();
		Thread thread1 = new AcquirerThread(1, thread1AccquireLock, thread1ReleaseLock);
		Thread thread2 = new AcquirerThread(1, thread2AccquireLock, thread2ReleaseLock);
		// Then
		thread1ReleaseLock.lock();
		thread2AccquireLock.lock();
		thread2ReleaseLock.lock();
		thread1.start();
		thread2.start();
		Thread.sleep(waitTimeMs);	// thread1 is the owner of L1
		thread2AccquireLock.unlock();	// threads tries to get L1 and gets blocked
		Thread.sleep(waitTimeMs);
		assertEquals(thread1.getState(), Thread.State.WAITING);
		assertEquals(thread2.getState(), Thread.State.BLOCKED);
		thread1ReleaseLock.unlock();	// thread1 releases  L1 and thread2 takes its ownership
		Thread.sleep(waitTimeMs);
		assertEquals(thread1.getState(), Thread.State.TERMINATED);
		assertEquals(thread2.getState(), Thread.State.WAITING);
		assertTrue(locks.getLocks().isEmpty());
		thread2ReleaseLock.unlock();	// thread2 releases L1
		Thread.sleep(waitTimeMs);
		assertEquals(thread2.getState(), Thread.State.TERMINATED);
 		assertTrue(locks.getLocks().isEmpty());
	}

	@Test(timeout = 200)
	public void testSyncronizedAccessWithDistinctIds() throws InterruptedException {
		// Given
		Lock thread1AccquireLock = new ReentrantLock();
		Lock thread1ReleaseLock = new ReentrantLock();
		Lock thread2AccquireLock = new ReentrantLock();
		Lock thread2ReleaseLock = new ReentrantLock();
		Thread thread1 = new AcquirerThread(1, thread1AccquireLock, thread1ReleaseLock);
		Thread thread2 = new AcquirerThread(2, thread2AccquireLock, thread2ReleaseLock);
		// Then
		thread1.start();
		thread2.start();
		Thread.sleep(waitTimeMs);
		assertEquals(thread1.getState(), Thread.State.TERMINATED);
		assertEquals(thread2.getState(), Thread.State.TERMINATED);
 		assertTrue(locks.getLocks().isEmpty());
	}

	private class AcquirerThread extends Thread {

		private long id;
		private Lock accquireLock;
		private Lock releaseLock;

		public AcquirerThread(long id, Lock accquireLock, Lock releaseLock) {
			this.id = id;
			this.accquireLock = accquireLock;
			this.releaseLock = releaseLock;
		}

		@Override
		public void run() {
			accquireLock.lock();
			locks.atomic(id, () -> {
				releaseLock.lock();
				return null;
			});
		}
	}

}
