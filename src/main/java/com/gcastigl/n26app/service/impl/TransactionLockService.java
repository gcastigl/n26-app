package com.gcastigl.n26app.service.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TransactionLockService {

	private static final Logger logger = LoggerFactory.getLogger(TransactionLockService.class);

	private final ConcurrentHashMap<Long, Object> locks = new ConcurrentHashMap<>();
	private final Function<Long, Object> valueSupplier = (id) -> new Object();

	public Map<Long, Object> getLocks() {
		return locks;
	}

	public <T> T atomic(long id, Supplier<T> supplier) {
		T result;
		synchronized (locks.computeIfAbsent(id, valueSupplier)) {
			logger.debug(String.format("Thread %d is the owner of %d", Thread.currentThread().getId(), id));
			try {
				result = supplier.get();
			} finally {
				logger.debug(String.format("Thread %d releasing %d", Thread.currentThread().getId(), id));
				locks.remove(id);
			}
		}
		return result;
	}

}
