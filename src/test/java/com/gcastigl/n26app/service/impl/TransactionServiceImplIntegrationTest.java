package com.gcastigl.n26app.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import com.gcastigl.n26app.model.io.TransactionRequest;
import com.gcastigl.n26app.service.api.TransactionService;
import com.gcastigl.n26app.service.api.TransactionService.TransactionOperationError;

import jersey.repackaged.com.google.common.collect.Sets;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TransactionServiceImplIntegrationTest {

	@Autowired
	private TransactionService transactions;

	@Test
	public void simpleChainedTransaitionCretionTest() {
		Optional<TransactionOperationError> result1 = transactions.create(10L, TransactionRequest.with(5000, "cars", null));
		Optional<TransactionOperationError> result2 = transactions.create(11L, TransactionRequest.with(10000, "shopping", 10L));
		assertFalse(result1.isPresent());
		assertFalse(result2.isPresent());
		assertEquals(transactions.findAllByType("cars"), Sets.newHashSet(10L));
		assertEquals(transactions.getSum(10L).get().getSum(), 15000, 0.01);
		assertEquals(transactions.getSum(11L).get().getSum(), 10000, 0.01);
	}

}
