package com.gcastigl.n26app.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.gcastigl.n26app.web.TransactionResource;

@Configuration
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		register(TransactionResource.class);
	}
}
