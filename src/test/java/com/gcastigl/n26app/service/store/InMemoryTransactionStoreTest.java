package com.gcastigl.n26app.service.store;

import static com.google.common.collect.Iterables.getOnlyElement;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.function.Supplier;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.gcastigl.n26app.model.entity.Transaction;
import com.gcastigl.n26app.service.impl.TransactionLockService;
import com.google.common.collect.Sets;

@RunWith(MockitoJUnitRunner.class)
public class InMemoryTransactionStoreTest {

	@Mock
	private TransactionLockService locks;
	
	@InjectMocks
	private InMemoryTransactionStore store;

	@SuppressWarnings("unchecked")
	@Before
	public <T> void setup() {
		when(locks.atomic(anyLong(), any(Supplier.class))).then(new Answer<Object>() {
			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(1, Supplier.class).get();
			}
		});
	}
	
	@Test
	public void testSaveAndFindById() {
		// Given
		Long id = 1L;
		String type = "asd";
		BigDecimal amount = new BigDecimal(100);
		Transaction t = new Transaction(id, type, amount, amount, null);
		t.setVersion(2);
		// When
		// Then
		store.save(t);
		assertEquals(t, store.findById(id));
		assertEquals(id, getOnlyElement(store.findAllByType(type)));
		assertTrue(store.exists(id));
	}

	@Test
	public void testSaveAndFindByType() {
		// Given
		String type1 = "asd1";
		String type2 = "asd2";
		BigDecimal amount = new BigDecimal(100);
		Transaction t11 = new Transaction(1, type1, amount, amount, null);
		Transaction t12 = new Transaction(2, type1, amount, amount, null);
		Transaction t13 = new Transaction(3, type1, amount, amount, null);
		Transaction t21 = new Transaction(4, type2, amount, amount, null);
		Transaction t22 = new Transaction(5, type2, amount, amount, null);
		// When
		// Then
		store.save(t11);
		store.save(t12);
		store.save(t13);
		store.save(t21);
		store.save(t22);
		assertEquals(
			Sets.newHashSet(t11.getId(), t12.getId(), t13.getId()), store.findAllByType(type1)
		);
		assertEquals(
			Sets.newHashSet(t21.getId(), t22.getId()), store.findAllByType(type2)
		);
	}

}
