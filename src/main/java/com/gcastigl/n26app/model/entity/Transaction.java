package com.gcastigl.n26app.model.entity;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Transaction {

	private long id;
	private String type;
	private BigDecimal amount;
	private BigDecimal totalAmount;
	private Long parentId;
	private long version;

	Transaction() {
	}

	public Transaction(long id, String type, BigDecimal amount, BigDecimal totalAmount, Long parentId) {
		this.id = requireNonNull(id);
		this.type = requireNonNull(type);
		this.amount = requireNonNull(amount);
		this.totalAmount = requireNonNull(totalAmount);
		this.parentId = parentId;
		this.version = 1;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.MULTI_LINE_STYLE);
	}

	@Override
	public Transaction clone() {
		Transaction clone = new Transaction(id, type, amount, totalAmount, parentId);
		clone.version = version;
		return clone;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Transaction)) {
			return false;
		}
		Transaction other = (Transaction) obj;
		return new EqualsBuilder()
			.append(type, other.type)
			.append(amount, other.amount)
			.append(totalAmount, other.totalAmount)
			.append(parentId, other.parentId)
			.append(version, other.version)
			.build();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
			.append(type)
			.append(amount)
			.append(totalAmount)
			.append(parentId)
			.append(version)
			.build();
	}
}
