package com.gcastigl.n26app.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.gcastigl.n26app.model.entity.Transaction;
import com.gcastigl.n26app.model.io.TranasctionSum;
import com.gcastigl.n26app.model.io.TransactionRequest;
import com.gcastigl.n26app.service.api.TransactionService.TransactionOperationError;
import com.gcastigl.n26app.service.store.InMemoryTransactionStore;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplTest {

	@Mock
	private InMemoryTransactionStore store;

	@Spy
	@InjectMocks
	private TransactionServiceImpl transactions;

	@Test
	public void testCreateWithNullRequest() {
		Optional<TransactionOperationError> error = transactions.create(1L, null);
		assertTrue(error.isPresent());
		assertEquals(error.get(), TransactionOperationError.MISSING_PARAMETERS);
	}


	@Test
	public void createTransactionPointingToSelfShouldFail() {
		// Givem
		Long id = 10L;
		// When
		when(store.exists(id)).thenReturn(false);
		// Then
		Optional<TransactionOperationError> error = transactions.create(id, TransactionRequest.with(100, "asd", id));
		assertTrue(error.isPresent());
		assertEquals(error.get(), TransactionOperationError.INVALID_PARENT_ID);
	}

	@Test
	public void testCreateWithVeryLongType() {
		// Given
		String veryLongType = new String(new char[TransactionServiceImpl.TYPE_MAX_LEN + 1]).replace('\0', ' ');
		TransactionRequest request = mock(TransactionRequest.class);
		// When
		when(request.getType()).thenReturn(veryLongType);
		Optional<TransactionOperationError> error = transactions.create(1L, request);
		assertTrue(error.isPresent());
		assertEquals(error.get(), TransactionOperationError.INVALID_TYPE);
	}

	@Test
	public void testCreateOrphanTransaction() {
		// Given
		Long id = 1L;
		TransactionRequest request = TransactionRequest.with(100, "cars", null);
		// When
		when(store.exists(id)).thenReturn(false);
		// Then
		Optional<TransactionOperationError> error = transactions.create(id, request);
		assertFalse(error.isPresent());
		verify(store, times(1)).save(argThat(new ArgumentMatcher<Transaction>() {
			@Override
			public boolean matches(Object argument) {
				Transaction t = (Transaction) argument;
				return t.getType().equals(request.getType())
					&& t.getAmount().equals(new BigDecimal(request.getAmount()))
					&& t.getParentId() == null;
			}
		}));
	}

	@Test
	public void testCreateTransactionWithUnexistentParent() {
		// Given
		Long id = 1L;
		TransactionRequest request = TransactionRequest.with(100, "cars", 999L);
		// When
		when(store.exists(id)).thenReturn(false);
		// Then
		Optional<TransactionOperationError> error = transactions.create(id, request);
		assertTrue(error.isPresent());
		assertEquals(error.get(), TransactionOperationError.INVALID_PARENT_ID);
	}

	@Test
	public void testCreateTransactionWithParent() {
		// Given
		Long id = 1L;
		Long parentId = 2L;
		TransactionRequest request = TransactionRequest.with(100, "cars", parentId);
		BigDecimal parentTotalAmount = new BigDecimal(200);
		Transaction parent = mock(Transaction.class);
		// When
		when(store.exists(id)).thenReturn(false);
		when(parent.getId()).thenReturn(parentId);
		when(parent.getParentId()).thenReturn(null);
		when(parent.getTotalAmount()).thenReturn(parentTotalAmount);
		when(store.findById(parentId)).thenReturn(parent);
		when(store.save(any(Transaction.class))).thenAnswer(new Answer<Transaction>() {
			@Override
			public Transaction answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, Transaction.class);
			}
		});
		// Then
		Optional<TransactionOperationError> error = transactions.create(id, request);
		assertFalse(error.isPresent());
		verify(parent, times(1)).setTotalAmount(
			parentTotalAmount.add(new BigDecimal(request.getAmount()))
		);
		verify(store, times(2)).save(any(Transaction.class));
	}

	@Test
	public void testAllParentAmountsUpdated() {
		Long id = 1L;
		Long fatherId = 2L;
		Long grandFatherId = 3L;
		BigDecimal fatherTotalAmount = new BigDecimal(100);
		BigDecimal grandfatherTotalAmount = fatherTotalAmount.add(new BigDecimal(50));
		Transaction father = mock(Transaction.class);
		Transaction grandfather = mock(Transaction.class);
		BigDecimal requestAmount = new BigDecimal(222);
		TransactionRequest request = TransactionRequest.with(requestAmount.doubleValue(), "cars", fatherId);
		// When
		when(store.exists(id)).thenReturn(false);
		when(store.findById(fatherId)).thenReturn(father);
		when(store.findById(grandFatherId)).thenReturn(grandfather);
		when(store.save(any(Transaction.class))).thenAnswer(new Answer<Transaction>() {
			@Override
			public Transaction answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, Transaction.class);
			}
		});
		when(father.getId()).thenReturn(fatherId);
		when(father.getTotalAmount()).thenReturn(fatherTotalAmount);
		when(father.getParentId()).thenReturn(grandFatherId);
		when(father.clone()).thenCallRealMethod();
		when(grandfather.getId()).thenReturn(grandFatherId);
		when(grandfather.getTotalAmount()).thenReturn(grandfatherTotalAmount);
		when(grandfather.getParentId()).thenReturn(null);
		// Then
		Optional<TransactionOperationError> error = transactions.create(id, request);
		assertFalse(error.isPresent());
		verify(father, times(1)).setTotalAmount(fatherTotalAmount.add(requestAmount));
		verify(grandfather, times(1)).setTotalAmount(grandfatherTotalAmount.add(requestAmount));
	}

	@Test
	public void testFindByType() {
		// Given
		String type = "test";
		Set<Long> existingTypes = Collections.singleton(15L);
		// When
		when(store.findAllByType(eq(type))).thenReturn(existingTypes);
		// Then
		Set<Long> result = transactions.findAllByType(type);
		assertEquals(result, existingTypes);
	}

	@Test
	public void testGetSum() {
		// Given
		Long id = 19L;
		Transaction transaction = mock(Transaction.class);
		BigDecimal totalAmount = new BigDecimal(100);
		// When
		when(store.findById(id)).thenReturn(transaction);
		when(transaction.getTotalAmount()).thenReturn(totalAmount);
		// Then
		Optional<TranasctionSum> present = transactions.getSum(id);
		Optional<TranasctionSum> empty = transactions.getSum(id + 1);
		assertEquals(present.get().getSum(), totalAmount.doubleValue(), 0.01);
		assertTrue(!empty.isPresent());
	}

}
