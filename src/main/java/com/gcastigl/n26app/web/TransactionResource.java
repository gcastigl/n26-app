package com.gcastigl.n26app.web;

import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcastigl.n26app.model.io.OperationResult;
import com.gcastigl.n26app.model.io.TranasctionSum;
import com.gcastigl.n26app.model.io.TransactionRequest;
import com.gcastigl.n26app.service.api.AppServices;
import com.gcastigl.n26app.service.api.TransactionService.TransactionOperationError;

@Component
@Path("/transactionservice")
public class TransactionResource {

	private static final Logger logger = LoggerFactory.getLogger(TransactionResource.class);

	@Autowired
	private AppServices app;

	@PUT
	@Path("/transaction/{transactionId}")
	@Produces("application/json")
	public OperationResult create(@PathParam("transactionId") Long transactionId, TransactionRequest request) {
		try {
			return app.transactions().create(transactionId, request)
				.map(error -> OperationResult.error(error)).orElse(OperationResult.ok());
		} catch (Throwable e) {
			logUnexpectedException(e);
			return OperationResult.error(TransactionOperationError.UNEXPECTED);
		}
	}

	@GET
	@Path("/types/{type}")
	@Produces("application/json")
	public Collection<Long> getTypes(@PathParam("type") String type) {
		try {
			return app.transactions().findAllByType(type);
		} catch (Throwable e) {
			logUnexpectedException(e);
			return null;
		}
	}

	@GET
	@Path("/sum/{transactionId}")
	@Produces("application/json")
	public TranasctionSum getSum(@PathParam("transactionId") Long transactionId) {
		try {
			return app.transactions().getSum(transactionId).orElse(null);
		} catch (Throwable e) {
			logUnexpectedException(e);
			return null;
		}
	}

	void logUnexpectedException(Throwable e) {
		logger.error("Unexpected exception", e);
	}
}
