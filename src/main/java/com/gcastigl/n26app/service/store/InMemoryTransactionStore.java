package com.gcastigl.n26app.service.store;

import static java.util.Collections.emptySet;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcastigl.n26app.model.entity.Transaction;
import com.gcastigl.n26app.service.impl.TransactionLockService;

@Component
public class InMemoryTransactionStore {

	private static final Logger logger = LoggerFactory.getLogger(InMemoryTransactionStore.class);

	@Autowired
	private TransactionLockService locks;

	private Map<Long, Transaction> transactionsById = new HashMap<>();
	private Map<String, Set<Long>> transactionsByType = new HashMap<>();

	public boolean exists(long id) {
		return transactionsById.containsKey(id);
	}

	public Transaction save(Transaction transaction) {
		requireNonNull(transaction);
		requireNonNull(transaction.getId());
		long id = transaction.getId();
		Transaction clone = transaction.clone();
		return locks.atomic(id, () -> {
			Transaction existing = transactionsById.get(id);
			if (!isNull(existing)) {
				if (existing.getVersion() > clone.getVersion()) {
					throw new OptimisticLockException();
				}
			} else {
				clone.setVersion(1);
			}
			clone.setVersion(clone.getVersion() + 1);
			transactionsById.put(id, clone);
			Set<Long> types = transactionsByType.get(clone.getType());
			if (types == null) {
				transactionsByType.put(clone.getType(), types = new HashSet<>());
			}
			types.add(id);
			logger.debug("Thread %d updated transaction %d", Thread.currentThread().getId(), id);
			return findById(clone.getId());
		});
	}

	public Set<Long> findAllByType(String type) {
		return transactionsByType.getOrDefault(type, emptySet());
	}

	public Transaction findById(Long transactionId) {
		Transaction result = transactionsById.get(transactionId);
		return isNull(result) ? null : result.clone();
	}

}
