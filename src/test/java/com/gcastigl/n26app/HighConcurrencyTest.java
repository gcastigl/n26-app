package com.gcastigl.n26app;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.gcastigl.n26app.model.io.OperationResult;
import com.gcastigl.n26app.model.io.OperationResult.StatusType;
import com.gcastigl.n26app.model.io.TranasctionSum;
import com.gcastigl.n26app.model.io.TransactionRequest;
import com.gcastigl.n26app.util.Threads;

/**
 * Make sure to have a version of the application running at localhost:8080 before executing this class 
 */
public class HighConcurrencyTest {

	private static final String BASE_URL = "http://localhost:8080/transactionservice";
	private static final int N = 5000;

	public static void main(String[] args) throws RestClientException, URISyntaxException, InterruptedException {
		new HighConcurrencyTest().run();
	}

	public void run() throws RestClientException, URISyntaxException, InterruptedException {
		RestTemplate rt = new RestTemplate();
		rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		rt.getMessageConverters().add(new StringHttpMessageConverter());
		concurrentlyCreateTransactions(rt);
		assertEquals((int) getTransactionSum(rt, 1).getSum(), N);
		assertEquals((int) getType(rt, "pair").size(), N / 2);
		assertEquals((int) getType(rt, "odd").size(), N / 2);
	}

	private void concurrentlyCreateTransactions(RestTemplate rt) throws InterruptedException {
		String[] types = { "pair", "odd" };
		ExecutorService executor = Executors.newFixedThreadPool(5);
		for (int i = 1; i <= N; i++) {
			long n = i;
			executor.execute(() -> {
				Threads.sleepFor((long) (Math.random() * 50), TimeUnit.MILLISECONDS);
				boolean success;
				do {
					long id = n;
					Long parentId = n == 1 ? null : n / 2;
					double amount = 1;
					String type = types[(int) n % 2];
					OperationResult result = create(rt, id, TransactionRequest.with(amount, type, parentId));
					success = !result.getStatus().equals(StatusType.ERROR);
					if (!success) {
						Threads.sleepFor((long) (Math.random() * 50), TimeUnit.MILLISECONDS);
					}
					System.out.println(String.format("Node %d created", n));
				} while (!success);
			});
		}
		executor.shutdown();
		while (!executor.isTerminated()) {
			Thread.sleep(500);
		}
		System.out.println("Finished creation of transactions");
	}

	public OperationResult create(RestTemplate rt, long id, TransactionRequest request) {
		String uri = String.format("%s/transaction/%d", BASE_URL, id);
		HttpEntity<TransactionRequest> requestEntity = new HttpEntity<>(request);
		HttpEntity<OperationResult> response = rt.exchange(uri, HttpMethod.PUT, requestEntity, OperationResult.class);
		return response.getBody();
	}

	@SuppressWarnings("unchecked")
	public List<Long> getType(RestTemplate rt, String type) throws RestClientException, URISyntaxException {
		String uri = String.format("%s/types/%s", BASE_URL, type);
		return (List<Long>) rt.getForObject(new URI(uri), List.class);
	}

	public TranasctionSum getTransactionSum(RestTemplate rt, long id) throws RestClientException, URISyntaxException {
		String uri = String.format("%s/sum/%s", BASE_URL, id);
		return rt.getForObject(new URI(uri), TranasctionSum.class);
	}
}
