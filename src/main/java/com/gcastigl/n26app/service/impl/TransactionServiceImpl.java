package com.gcastigl.n26app.service.impl;

import static java.util.Objects.isNull;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcastigl.n26app.model.entity.Transaction;
import com.gcastigl.n26app.model.io.TranasctionSum;
import com.gcastigl.n26app.model.io.TransactionRequest;
import com.gcastigl.n26app.service.api.TransactionService;
import com.gcastigl.n26app.service.store.InMemoryTransactionStore;
import com.gcastigl.n26app.service.store.OptimisticLockException;

@Service
public class TransactionServiceImpl implements TransactionService {

	public static final int TYPE_MAX_LEN = 512;

	@Autowired
	private InMemoryTransactionStore store;

	@Override
	public Optional<TransactionOperationError> create(long id, TransactionRequest request) {
		if (isNull(request)) {
			return Optional.of(TransactionOperationError.MISSING_PARAMETERS);
		}
		String type = request.getType();
		if (isNull(type) || type.length() > TYPE_MAX_LEN) {
			return Optional.of(TransactionOperationError.INVALID_TYPE);
		}
		BigDecimal amount = new BigDecimal(request.getAmount());
		if (amount.compareTo(BigDecimal.ZERO) <= 0) {
			return Optional.of(TransactionOperationError.INVALID_AMOUNT);
		}
		if (store.exists(id)) {
			return Optional.of(TransactionOperationError.INVALID_TRANSACTION_ID);
		}
		BigDecimal totalAmount = amount;
		Long parentId = request.getParent_id();
		if (!isNull(parentId)) {
			if (parentId.longValue() == id) {
				return Optional.of(TransactionOperationError.INVALID_PARENT_ID);
			}
			Transaction parent = store.findById(parentId);
			if (isNull(parent)) {
				return Optional.of(TransactionOperationError.INVALID_PARENT_ID);
			}
			addAmountToAllParents(parent, amount);
		}
		store.save(new Transaction(id, type, amount, totalAmount, parentId));
		return Optional.empty();
	}

	private Transaction addAmountToAllParents(Transaction transaction, BigDecimal amount) {
		if (transaction == null) {
			return null;
		}
		transaction = incrementTotal(transaction.getId(), amount);
		if (!isNull(transaction.getParentId())) {
			addAmountToAllParents(store.findById(transaction.getParentId()), amount);
		}
		return transaction;
	}

	public Transaction incrementTotal(long id, BigDecimal increment) {
		Transaction transaction;
		boolean updated = false;
		do {
			try {
				transaction = store.findById(id);
				transaction.setTotalAmount(transaction.getTotalAmount().add(increment));
				return store.save(transaction);
			} catch (OptimisticLockException e) {
				
			}
		} while (!updated);
		throw new IllegalStateException("Should never get here");
	}

	@Override
	public Set<Long> findAllByType(String type) {
		return store.findAllByType(type);
	}

	@Override
	public Optional<TranasctionSum> getSum(Long transactionId) {
		return Optional.ofNullable(store.findById(transactionId))
				.map(t -> new TranasctionSum(t.getTotalAmount().doubleValue()));
	}

}
