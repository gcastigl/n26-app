package com.gcastigl.n26app.util;

import java.util.concurrent.TimeUnit;

public class Threads {

	public static void sleepFor(long duration, TimeUnit unit) {
		try {
			Thread.sleep(unit.toMillis(duration));
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}
	}
}
