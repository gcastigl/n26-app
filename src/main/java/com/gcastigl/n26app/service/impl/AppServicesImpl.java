package com.gcastigl.n26app.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcastigl.n26app.service.api.AppServices;
import com.gcastigl.n26app.service.api.TransactionService;

@Service
public class AppServicesImpl implements AppServices {

	@Autowired
	private TransactionService transactions;

	public TransactionService transactions() {
		return transactions;
	}

}
